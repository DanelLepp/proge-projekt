#########################################################################
# Matevalemite kogum
# Plaanis on, et oleks rakendus kus saab õppida mugavalt kõiki valemeid
# Edasiarendused:
# loogiline puu
# valemid
# valemite n2ited
# valemite otsing
# testid valemite peale
#########################################################################

from tkinter import *
from tkinter import ttk
from tkinter import messagebox

def kt1_teemad():
    uus_aken()
    raamkt1 = Tk()
    raamkt1.title('Maatriksid ja determinandid')
    raamkt1.geometry('350x200')
    nuppkt1 = ttk.Button(raamkt1, text='Valem')
    nuppkt1.place(x=70, y=40, width=200)
    tagasi_nupp = ttk.Button(raamkt1, text='<-- Tagasi', command = raam)
    tagasi_nupp.place(x=5, y=5, width=100)
    
#def kt2_teemad():
    
#def kt3_teemad():
    
def tuletiste_tabel():
    raamkt1.destroy() 
    raam = Tk()
    raam.title('Tuletiste tabel')
    c = ttk.Label(raam, text = 'c\' = 0')
    c.place(x=5, y=5)
    ruutjuur = ttk.Label(raam, text = '') 
    ruutjuur.place(x=5, y=25)

def uus_aken():
    command=raam.destroy()

#loob akna
raam = Tk()
raam.title('Kõrgem matemaatika')
raam.geometry('680x250')

#kiri akna servas
#silt = ttk.Label(raam, text='')
#silt.place(x=5, y=5)

#esimene veerg ja silt
silt = ttk.Label(raam, text='KT 1 põhiteemad')
silt.place(x=20, y=40)

#loob nupud
nupp1 = ttk.Button(raam, text='Maatriksid', command=kt1_teemad)
nupp1.place(x=20, y=70, width=200)

nupp2 = ttk.Button(raam, text='Determinandid')
nupp2.place(x=20, y=100, width=200)

nupp3 = ttk.Button(raam, text='Lineaarvõrrandite süsteemid')
nupp3.place(x=20, y=130, width=200)

nupp4 = ttk.Button(raam, text='Funktsioonid')
nupp4.place(x=20, y=160, width=200)

#teine veerg ja silt 
silt = ttk.Label(raam, text='KT 2 põhiteemad')
silt.place(x=240, y=40)

#loob nupud
nupp1 = ttk.Button(raam, text='Teema 1', command=kt1_teemad)
nupp1.place(x=240, y=70, width=200)

nupp2 = ttk.Button(raam, text='Teema 2')
nupp2.place(x=240, y=100, width=200)

nupp3 = ttk.Button(raam, text='Teema 3')
nupp3.place(x=240, y=130, width=200)

nupp4 = ttk.Button(raam, text='Teema 4')
nupp4.place(x=240, y=160, width=200)

#kolmas veerg ja silt 
silt = ttk.Label(raam, text='KT 3 põhiteemad')
silt.place(x=460, y=40)

#loob nupud
nupp1 = ttk.Button(raam, text='Teema 1', command=kt1_teemad)
nupp1.place(x=460, y=70, width=200)

nupp2 = ttk.Button(raam, text='Teema 2')
nupp2.place(x=460, y=100, width=200)

nupp3 = ttk.Button(raam, text='Teema 3')
nupp3.place(x=460, y=130, width=200)

nupp4 = ttk.Button(raam, text='Teema 4')
nupp4.place(x=460, y=160, width=200)

raam.mainloop()

#p6hi_aken()
